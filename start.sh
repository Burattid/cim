#!/bin/bash
cd /usr/src/app/cim

# Start Gunicorn processes
echo Starting Gunicorn.
exec gunicorn be.wsgi:application \
    --bind 0.0.0.0:8000 \
    --workers 3 \
    --reload