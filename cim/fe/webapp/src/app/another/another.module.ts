import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsModule, AccordionModule, AlertComponent } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';
import { SortableModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';



import { DndModule } from 'ngx-drag-drop';
import { NgxDnDModule } from '@swimlane/ngx-dnd';


import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';


import { AnotherComponent } from './another.component';

import { CimApiService } from '../services/cim.service';


export const routes = [
  { path: '', component: AnotherComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    BsDropdownModule,
    SharedModule,
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    DndModule,
    NgxDnDModule,
    SortableModule.forRoot(),
    ModalModule.forRoot()
    
  ],
  declarations: [AnotherComponent],
  providers: [CimApiService],
})
export class AnotherModule {
  static routes = routes;
}