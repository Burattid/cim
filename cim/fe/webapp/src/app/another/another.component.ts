import { Component, TemplateRef } from '@angular/core';
import { CimApiService } from '../services/cim.service';
import { DndDropEvent } from 'ngx-drag-drop';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


declare let jQuery: any;


@Component({
    selector: 'app-another',
    templateUrl: './another.template.html',
    styleUrls: ['./another.component.css']

})
export class AnotherComponent {

    rForm: FormGroup;
    modalRef: BsModalRef;
    myControl = new FormControl();

    constructor(private _demoService: CimApiService, private modalService: BsModalService) {

    }
    ngOnInit() {
        /* TODO        
        * Varie chiamate AJAX per ottenere i valori dei filtri
        * (es. anni stagione, classi, ecc...)
        */

    }

    public isLoading: boolean = false;
    public attivo = false;

    public isError: boolean = false;
    message: string;

    /**
     * Memorizza solamente l'id delle selezioni degli autocomplete (i cui valori 
     * sono nella forma "ID - <descrizione>")
     */
    idPreso = "";

    attivaBtnCerca = false;

    /**
     * Variabili temporanee, in attesa di poter comunicare col backend.
     */
    public listaTemplate = [
        {
            "nome": "modelli",
            "campi": [
                {
                    "nome": "anno_stagione",
                    "required": true
                },
                {
                    "nome": "classe",
                    "required": true
                },
                {
                    "nome": "segmento",
                    "required": false
                },
                {
                    "nome": "tipo_prova",
                    "required": false
                }
            ]
        },
        {
            "nome": "articoli",
            "campi": [
                {
                    "nome": "anno_stagione",
                    "required": true
                },
                {
                    "nome": "segmento",
                    "required": false
                },
                {
                    "nome": "gm",
                    "required": true
                },
                {
                    "nome": "tipo_prova",
                    "required": false
                }
            ]
        },
        {
            "nome": "imputazioni",
            "campi": [
                {
                    "nome": "imputazione",
                    "required": true
                }
            ]
        }
    ]

    arrayModelli = [];
    arrayArticoli = [];
    arrayImputazioni = [];

    gruppo?: string;
    societa?: string;
    classe?: string;
    gm?: string;

    confAnnoStagione = {
        defaultValue: "Anno Stagione",
        values: [
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2011 A/I",
                "valore": "2011 A/I",
                "ordinamento": 20112,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2011 P/E",
                "valore": "2011 P/E",
                "ordinamento": 20111,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2012 A/I",
                "valore": "2012 A/I",
                "ordinamento": 20122,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2012 P/E",
                "valore": "2012 P/E",
                "ordinamento": 20121,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2013 A/I",
                "valore": "2013 A/I",
                "ordinamento": 20132,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2013 P/E",
                "valore": "2013 P/E",
                "ordinamento": 20131,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2014 A/I",
                "valore": "2014 A/I",
                "ordinamento": 20142,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2014 P/E",
                "valore": "2014 P/E",
                "ordinamento": 20141,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2015 A/I",
                "valore": "2015 A/I",
                "ordinamento": 20152,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2015 P/E",
                "valore": "2015 P/E",
                "ordinamento": 20151,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2016 A/I",
                "valore": "2016 A/I",
                "ordinamento": 20162,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2016 P/E",
                "valore": "2016 P/E",
                "ordinamento": 20161,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2017 A/I",
                "valore": "2017 A/I",
                "ordinamento": 20172,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2017 P/E",
                "valore": "2017 P/E",
                "ordinamento": 20171,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2018 A/I",
                "valore": "2018 A/I",
                "ordinamento": 20182,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2018 P/E",
                "valore": "2018 P/E",
                "ordinamento": 20181,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2019 A/I",
                "valore": "2019 A/I",
                "ordinamento": 20192,
                "upd_ts": "2018-08-01 17:36:48",
                "upd_user": ""
            },
            {
                "tipo": "anno_stagione_tagli",
                "chiave": "2019 P/E",
                "valore": "2019 P/E",
                "ordinamento": 20191,
                "upd_ts": "2017-12-12 09:59:25",
                "upd_user": ""
            }
        ],
        includeAllItem: false,
        required: true,
        campo: "valore"
    };
    confTipoProva = {
        defaultValue: "Tipo Prova",
        values: [
            {
                "tipo": "tipologia_prove_cim",
                "chiave": "Certificazione",
                "valore": "Certificazione/Composizione",
                "ordinamento": 0,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "tipologia_prove_cim",
                "chiave": "EcoTox",
                "valore": "EcoTossicologia",
                "ordinamento": 1,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "tipologia_prove_cim",
                "chiave": "Fisica",
                "valore": "Fisico-Tecnica",
                "ordinamento": 2,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "tipologia_prove_cim",
                "chiave": "Lavaggio",
                "valore": "Lavaggio ",
                "ordinamento": 3,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "tipologia_prove_cim_old",
                "chiave": "Infiammabilita",
                "valore": "Infiammabilita'",
                "ordinamento": 4,
                "upd_ts": "2018-08-20 17:23:40",
                "upd_user": "cim3.0"
            }
        ],
        includeAllItem: true,
        required: false,
        nomeValoreTutti: "Tutte"

    };
    confSegmentiModelli = {
        defaultValue: "Segmento",
        values: [
            {
                "tipo": "segmento_codifiche",
                "chiave": "campionario",
                "valore": "campionario",
                "ordinamento": 0,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "segmento_codifiche",
                "chiave": "produzione",
                "valore": "produzione",
                "ordinamento": 0,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            }
        ],
        includeAllItem: true,
        required: true,
        nomeValoreTutti: "Qualunque"
    };
    confSegmentiArticoli = {
        defaultValue: "Segmento",
        values: [
            {
                "tipo": "segmento_mp",
                "chiave": "campionario",
                "valore": "Campionario",
                "ordinamento": 20,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "segmento_mp",
                "chiave": "capi_prova",
                "valore": "Capi prova",
                "ordinamento": 40,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "segmento_mp",
                "chiave": "inserimento",
                "valore": "Inserimento",
                "ordinamento": 50,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "segmento_mp",
                "chiave": "produzione",
                "valore": "Produzione",
                "ordinamento": 10,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            },
            {
                "tipo": "segmento_mp",
                "chiave": "sfilata",
                "valore": "Sfilata",
                "ordinamento": 30,
                "upd_ts": "2017-07-03 13:56:07",
                "upd_user": ""
            }
        ],
        includeAllItem: true,
        nomeValoreTutti: "Qualunque",
        required: true,
    };
    confSocieta = {
        name: "societa",
        defaultValue: "Max Mara",
        values: [{ valore: "Max Mara" }],
        includeAllItem: false,
        required: true,
    };
    confClasse = {
        nome: "classe",
        placeholder: 'Classe',
        values: [
            "01 - Cappotto",
            "04 - Giacca",
            "10 - Gonna",
            "22 - Abito",
            "46 - Pelliccia",
        ],
        required: true,
        template: "templateClasse",
    };

    confGM = {
        nome: "gm",
        placeholder: "GM",
        values: [
            "10 - Tessuto",
            "13 - Jersey",
            "29 - Lampo",
            "56 - Piumino",
            "84 - Tessuto per accessori",
            "81 - Pelle scarpe e borse"
        ],
        required: true,
        template: "templateGM",
    };

    confImputazione = {
        placeholder: "Imputazione",
        values: [
            {
                "chiave": "0",
                "valore": "Tessuto ricamato",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "1",
                "valore": "Fodera in",
                "ordinamento": "101",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "1A",
                "valore": "1^ Fodera",
                "ordinamento": "101",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "1F",
                "valore": "FODERA 1",
                "ordinamento": "132",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "2",
                "valore": "Tessuto ricamo",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "3",
                "valore": "Con particolari in",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "33",
                "valore": "con accessori in pellicceria",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "4",
                "valore": "Con cintura in",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "5",
                "valore": "Decorazioni in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "50",
                "valore": "Davanti",
                "ordinamento": "30",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "50A",
                "valore": "Davanti",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "51",
                "valore": "Dietro",
                "ordinamento": "31",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "51A",
                "valore": "Dietro",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "52",
                "valore": "Maniche",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "53",
                "valore": "Dettagli",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "54",
                "valore": "Decorazioni",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "55",
                "valore": "Bordi",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "57",
                "valore": "Ricamo escluso",
                "ordinamento": "130",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "58",
                "valore": "Capo di abbigliamento",
                "ordinamento": "132",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "6",
                "valore": "Con particolari in pelliccia",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "60",
                "valore": "Polsi",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "61",
                "valore": "con accessorio in",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "62",
                "valore": "Fondo",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "63",
                "valore": "Filato in maglia",
                "ordinamento": "7",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "64",
                "valore": "Tessuto a maglia",
                "ordinamento": "9",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "65",
                "valore": "Accessori Esclusi",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "66",
                "valore": "Tessuto a maglia 1",
                "ordinamento": "9",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "67",
                "valore": "Tessuto a maglia 2",
                "ordinamento": "9",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "68",
                "valore": "Tessuto a maglia 3",
                "ordinamento": "9",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "69",
                "valore": "Fodera parte superiore",
                "ordinamento": "11",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "7",
                "valore": "Parte superiore",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "70",
                "valore": "Fodera parte inferiore",
                "ordinamento": "11",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "71",
                "valore": "Altre parti",
                "ordinamento": "12",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "71A",
                "valore": "Altre parti",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "72",
                "valore": "Fodera fusto",
                "ordinamento": "13",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "73",
                "valore": "Fodera fusto",
                "ordinamento": "112",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "74",
                "valore": "Parte superiore esterna",
                "ordinamento": "17",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "75",
                "valore": "Parte superiore interna",
                "ordinamento": "17",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "76",
                "valore": "Tessuto a maglia costina",
                "ordinamento": "17",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "77",
                "valore": "Filato in maglia a pelo",
                "ordinamento": "17",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "78",
                "valore": "Frangia",
                "ordinamento": "17",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "79",
                "valore": "Fodera parte superiore",
                "ordinamento": "110",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "8",
                "valore": "Parte inferiore",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "80",
                "valore": "Fodera parte inferiore",
                "ordinamento": "111",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "81",
                "valore": "Parte centrale",
                "ordinamento": "39",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "82",
                "valore": "Parte superiore dietro",
                "ordinamento": "42",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "83",
                "valore": "Parte superiore davanti",
                "ordinamento": "45",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "84",
                "valore": "Parte centrale davanti",
                "ordinamento": "46",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "85",
                "valore": "Feltro",
                "ordinamento": "55",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "86",
                "valore": "Tessuto a navetta",
                "ordinamento": "56",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "88",
                "valore": "Tessuto a maglia 4",
                "ordinamento": "9",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "9",
                "valore": "Collo in pelo di",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "90",
                "valore": "Parte inferiore dietro",
                "ordinamento": "121",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "91",
                "valore": "Capo di abbigliamento",
                "ordinamento": "122",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "A",
                "valore": "Filo ricamo esterno",
                "ordinamento": "1005",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "A1",
                "valore": "Fodera in",
                "ordinamento": "1001",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "A2",
                "valore": "Capo di abbigliamento",
                "ordinamento": "53",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "AA",
                "valore": "intreccio in",
                "ordinamento": "18",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "AP",
                "valore": "con accessori in pellicceria",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "B",
                "valore": "Filo ricamo interno",
                "ordinamento": "1006",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "B1",
                "valore": "Borsa in",
                "ordinamento": "11",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "B2",
                "valore": "Bustino davanti",
                "ordinamento": "6",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "B3",
                "valore": "Bustino dietro",
                "ordinamento": "6",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "B4",
                "valore": "Bustino davanti",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "B5",
                "valore": "Bustino dietro",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "BB",
                "valore": "Borsa in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "BC",
                "valore": "con busta di custodia",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "BM",
                "valore": "Bustino in maglia",
                "ordinamento": "8",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "BO",
                "valore": "Bordo in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "BP",
                "valore": "Imbottitura piuma",
                "ordinamento": "134",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "BR",
                "valore": "Con bretelle in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "C",
                "valore": "Con",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "C1",
                "valore": "Cintura in",
                "ordinamento": "11",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "CC",
                "valore": "con",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "CL",
                "valore": "Collo",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "CT",
                "valore": "con cintura in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "D",
                "valore": "Fodera staccabile in",
                "ordinamento": "114",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "D1",
                "valore": "Parte inferiore dietro",
                "ordinamento": "52",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "DS",
                "valore": "Davanti tessuto esterno",
                "ordinamento": "11",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "E",
                "valore": "2^ Tessuto",
                "ordinamento": "12",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "F",
                "valore": "Tessuto di fondo",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "F1",
                "valore": "Filato in maglia 1",
                "ordinamento": "19",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "F2",
                "valore": "2^ Fodera",
                "ordinamento": "102",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "F3",
                "valore": "3^ Fodera",
                "ordinamento": "104",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "F4",
                "valore": "Filo decorativo esterno",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "F5",
                "valore": "Filo decorativo interno",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "F6",
                "valore": "Filato in maglia 2",
                "ordinamento": "20",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "F7",
                "valore": "Filato in maglia 3",
                "ordinamento": "21",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "F8",
                "valore": "Filato in maglia 4",
                "ordinamento": "22",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "F9",
                "valore": "Filato in maglia 5",
                "ordinamento": "23",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "FF",
                "valore": "Fodera",
                "ordinamento": "100",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "FG",
                "valore": "Filato in maglia",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "FI",
                "valore": "Filato",
                "ordinamento": "18",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "FL",
                "valore": "Filo di legatura",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "FM",
                "valore": "Fodera maniche",
                "ordinamento": "113",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "FO",
                "valore": "Fodera in",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "FU",
                "valore": "Foulard",
                "ordinamento": "51",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "G",
                "valore": "Gocce in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "H",
                "valore": "Tessuto esterno",
                "ordinamento": "16",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "HA",
                "valore": "Tulle",
                "ordinamento": "16",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "I",
                "valore": "Strato Ric. Sup. Int.",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "I1",
                "valore": "1^ Imbottitura ",
                "ordinamento": "135",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "I2",
                "valore": "2^ Imbottitura",
                "ordinamento": "136",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "IA",
                "valore": "Interfodera adesiva",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "IB",
                "valore": "con imbottitura in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "IF",
                "valore": "Imbottitura fibra",
                "ordinamento": "133",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "II",
                "valore": "interno in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "IM",
                "valore": "Imitazione Pelliccia",
                "ordinamento": "103",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "IP",
                "valore": "Con interno in pellicceria",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "IS",
                "valore": "Con interno staccabile",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "J",
                "valore": "3^ Tessuto",
                "ordinamento": "13",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "K",
                "valore": "Filato",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "L",
                "valore": "Maglia staccabile",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "LO",
                "valore": "Pelo 1",
                "ordinamento": "136",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "M",
                "valore": "Bustino in maglia",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "MA",
                "valore": "Maniche",
                "ordinamento": "50",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "MB",
                "valore": "Parti in Maglia",
                "ordinamento": "137",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "MG",
                "valore": "Con particolari in maglia",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "MM",
                "valore": "Modello in",
                "ordinamento": "10",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "MP",
                "valore": "Con polsi in maglia ",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "N",
                "valore": "Tessuto interno",
                "ordinamento": "18",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "N1",
                "valore": "Tessuto interno",
                "ordinamento": "1007",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "O",
                "valore": "Fiore in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "P",
                "valore": "Fodera-sottopiede in",
                "ordinamento": "100",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "P1",
                "valore": "Pizzo 1",
                "ordinamento": "136",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "P2",
                "valore": "Pezzo 2",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "P3",
                "valore": "Parte superiore davanti",
                "ordinamento": "115",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "P4",
                "valore": "Parte superiore dietro",
                "ordinamento": "116",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "P6",
                "valore": "PARTE CENTRALE",
                "ordinamento": "118",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "P7",
                "valore": "PARTE CENTRALE DAVANTI",
                "ordinamento": "119",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "P8",
                "valore": "PARTE CENTRALE DIETRO",
                "ordinamento": "120",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "PC",
                "valore": "con particolari in maglia",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "PE",
                "valore": "Pelle-Pelo",
                "ordinamento": "32",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "PI",
                "valore": "Parte inferiore",
                "ordinamento": "41",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "PP",
                "valore": "Con particolari in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "PS",
                "valore": "Parte superiore",
                "ordinamento": "40",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "PZ",
                "valore": "Pizzo in",
                "ordinamento": "42",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "Q",
                "valore": "Sciarpa",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "R",
                "valore": "Filo ricamo",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "R2",
                "valore": "2^ Filo ricamo",
                "ordinamento": "1002",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "R3",
                "valore": "3^ Filo ricamo",
                "ordinamento": "1003",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "R4",
                "valore": "Filo Decorativo",
                "ordinamento": "1004",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "S",
                "valore": "Strato ricoprente",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "SE",
                "valore": "Spalmature",
                "ordinamento": "10000",
                "gruppo": "speciale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "SF",
                "valore": "Stampa floccata",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "T",
                "valore": "Tomaia in",
                "ordinamento": "10",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "T1",
                "valore": "Tessuto 1",
                "ordinamento": "11",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "T5",
                "valore": "5^ Tessuto",
                "ordinamento": "15",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "TE",
                "valore": "Tessuto in",
                "ordinamento": "11",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "TEA",
                "valore": "Tessuto in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "TM",
                "valore": "Tessuto a maglia",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "TT",
                "valore": "Tessuto",
                "ordinamento": "11",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "U",
                "valore": "Suola in",
                "ordinamento": "101",
                "gruppo": "fodera",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "V",
                "valore": "Rinforzo in tessuto",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "VOD",
                "valore": "-",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "W",
                "valore": "Tess. accoppiati con",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "W1",
                "valore": "Accoppiatura",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "X",
                "valore": "Ricamo",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "XA",
                "valore": "2^ Ricamo",
                "ordinamento": "1001",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "Y",
                "valore": "4^ Tessuto",
                "ordinamento": "14",
                "gruppo": "principale",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            },
            {
                "chiave": "Z",
                "valore": "Pizzo in",
                "ordinamento": "1000",
                "gruppo": "aggiuntiva",
                "societa": "M006",
                "ins_ts": "2018-07-11 16:07:09",
                "ins_user": "cim2.0",
                "upd_ts": "2018-07-11 16:07:09",
                "upd_user": "cim2.0"
            }
        ],
        campo: "valore",
        campoRaggruppamento: "gruppo",
        template: "templateImputazioni",
        required: true,
        maxElemScroll: 10
    }


    svuotaTemplateModelli = () => {
        jQuery('#tabTemplateModelli tbody tr').each(function () {
            jQuery('#tabProveModelli tbody').append(jQuery(this));
        });
        jQuery('#btnSalvaTemplateModelli').addClass('is-hidden');
    }
    svuotaTemplateArticoli = () => {
        jQuery('#tabTemplateArticoli tbody tr').each(function () {
            jQuery('#tabProveArticoli tbody').append(jQuery(this));
        });
        jQuery('#btnSalvaTemplateArticoli').addClass('is-hidden');
    }
    svuotaTemplateImputazioni = () => {
        jQuery('#tabTemplateImputazioni tbody tr').each(function () {
            jQuery('#tabProveImputazioni tbody').append(jQuery(this));
        });
        jQuery('#btnSalvaTemplateImputazioni').addClass('is-hidden');
    }

    //  ------------------metodi relativi al DRAG & DROP 

    draggable = {
        data: "myDragData",
        effectAllowed: "move",
        disable: false,
        handle: false,
    };

    drag = (ev) => {
        //this.idPreso = ev.dataTransfer.setData("text", ev.target.id);
    }
    /**
     * Sposta la riga draggata nel tbody della tabella sulla quale è rilasciata.
     * 
     * N.B. Le righe delle tabelle DEVONO avere un ID, altrimenti non funziona.
     */
    onDrop = (ev) => {
        jQuery("#" + this.idPreso).toggleClass('modifica');
        jQuery(ev.event.target).closest('table').find('tbody').append(
            jQuery("#" + this.idPreso)
        );
    }
    onDragStart = (event) => {
        console.log("drag started", JSON.stringify(event, null, 2));
        this.idPreso = event.srcElement.getAttribute("id");

    }
    onDragEnd = (event: DragEvent) => {
        console.log("drag ended", JSON.stringify(event, null, 2));
    }
    onDragover = (event: DragEvent) => {
    }
    // ----------------------------------------------------
    
    
    /**
     * Esegue il filtraggio sulla tabella delle prove.
     */
    filtraProveModelli = () => {
        var value = jQuery('#filtroProveModelli').val().toLowerCase();
        jQuery("#tabProveModelli tbody tr").filter(function () {
            jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
        });
    }
    filtraProveArticoli = () => {
        var value = jQuery('#filtroProveArticoli').val().toLowerCase();
        jQuery("#tabProveArticoli tbody tr").filter(function () {
            jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
        });
    }
    filtraProveImputazioni = () => {
        var value = jQuery('#filtroProveImputazioni').val().toLowerCase();
        jQuery("#tabProveImputazioni tbody tr").filter(function () {
            jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
        });
    }


    getGruppo(val) {
        this.gruppo = val;
    }
    getClasse(val) {
        this.classe = val;
    }
    getGm(val) {
        this.gm = val;
    }
    getSocieta(val) {
        this.societa = val;
        this.attivaBtnCerca = true;
    }

    ricercaTemplateModelli = () => {

        if (jQuery('#tab1 .valido').length >= this.listaTemplate[0]["campi"].length) {
            this.arrayModelli = [
                {
                    "id": "44",
                    "nome_prova": "Test Infiammabilità",
                    "descrizione": "Questa è una descrizione di prova"
                },
                {
                    "id": "67",
                    "nome_prova": "Test Veleno",
                    "descrizione": "Questa è sparta"
                }
            ]
        }
        else {
            this.arrayModelli = []
        }
    }
    ricercaTemplateArticoli = () => {


        if (jQuery('#tab2 .valido').length >= this.listaTemplate[1]["campi"].length) {

            this.arrayArticoli = [
                {
                    "id": "44",
                    "nome_prova": "Test Infiammabilità",
                    "descrizione": "Questa è una descrizione di prova"
                },
                {
                    "id": "67",
                    "nome_prova": "Test Veleno",
                    "descrizione": "Questa è sparta"
                }
            ]
        }
        else {
            console.log("NON ci siamo");
            this.arrayArticoli = []
        }
    }
    ricercaTemplateImputazioni = () => {

        if (jQuery('#tab3 .valido').length >= this.listaTemplate[2]["campi"].length) {
            console.log("VALIDO");
            this.arrayImputazioni = [
                {
                    "id": "44",
                    "nome_prova": "Test Infiammabilità",
                    "descrizione": "Questa è una descrizione di prova"
                },
                {
                    "id": "67",
                    "nome_prova": "Test Veleno",
                    "descrizione": "Questa è sparta"
                }
            ]
        }
        else {
            console.log("NON ci siamo");
            this.arrayImputazioni = [];
        }
    }

    /**
     * Funzione per visualizzare l'alert di conferma del salvataggio.     
     */
    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
    }

    confermaSalvataggio(): void {
        this.message = 'Confirmed!';
        this.modalRef.hide();
    }
    annullaSalvataggio(): void {
        this.message = 'Declined!';
        this.modalRef.hide();
    }

}
