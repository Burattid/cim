import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap';
import { TypeaheadModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';



import {DropdownComponent} from './dropdown/dropdown.component'
import { AutocompleteComponent } from './autocomplete/autocomplete.component'
import { Autocomplete2Component } from './autocomplete2/autocomplete2.component'





@NgModule({
  imports: [
    CommonModule,
    BsDropdownModule,
    TypeaheadModule.forRoot(),
    FormsModule
  ],
  declarations: [
    DropdownComponent,
    AutocompleteComponent,
    Autocomplete2Component
    
  ],
  exports:[
    DropdownComponent,
    AutocompleteComponent,
    Autocomplete2Component
    
  ]
})
export class SharedModule { }
