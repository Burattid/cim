import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

interface IDropdownConfig {
    name?: string;
    defaultValue: string;
    values: any[];
    includeAllItem?: boolean;
    required?: boolean;
    campo?: string;
    nomeValoreBase?: string;
}

@Component({
    selector: 'dropdown',
    templateUrl: './dropdown.component.html',
    styleUrls: ['./dropdown.component.scss',]
})

export class DropdownComponent implements OnInit {
    _selectedValue: string;
    public valido: boolean = false;

    constructor() {
        //console.log("Lanciato CONTRUCTOR");
    }
    ngOnInit() {
        //console.log("config: " + JSON.stringify(this.config));
        //console.log("Lanciato ngOnInit");
        this._selectedValue = this.config.defaultValue;
    }


    @Input() config: IDropdownConfig = {
        'defaultValue': 'Dropdown',
        'values': [],

    };
    @Input() isFiltroTemplate: boolean = false;
    @Input() bordi: boolean = true;


    @Output() notifyChange: EventEmitter<string> = new EventEmitter<string>();
    @Output() cambioSocieta: EventEmitter<string> = new EventEmitter<string>();

    ChangeValue(newValue: string) {
        this._selectedValue = newValue ? newValue : this.config.defaultValue;
        //console.log("Dropdown value is changed to '" + this._selectedValue + "'! I'm emitting nofify!");
        this.notifyChange.emit(newValue);
    
        if (this.config.name == "societa"){
            this.cambioSocieta.emit(newValue);
        }

        if (this.config.includeAllItem == true || (this.config.includeAllItem == false && this._selectedValue != this.config.defaultValue)){
           this.valido = true;
        }
        else {
            this.valido = false;
        }
    }

}
