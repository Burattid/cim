import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';



/*
* Interfaccia che deve rispettare la variabile
* di configurazione da passare all'oggetto.
*/
interface IAutocomplete2Config {
  nome: string;
  values: any[];
  required?: boolean;
  placeholder?: string;
  campo?: string;
  campoRaggruppamento?: string;
  template?: string;
  maxElemScroll?: number;
}

@Component({
  selector: 'autocompleta-imputazioni',
  templateUrl: './autocomplete2.component.html',
  styleUrls: ['../shared.component.scss']
})

export class Autocomplete2Component implements OnInit {
  constructor() {

  }
  ngOnInit() {
    //console.log("config: " + JSON.stringify(this.config));
  }
  selected;
  selectedOption: any;
  isSelezione: boolean = false;
  gruppo?: string;



  @Input() config: IAutocomplete2Config = {
    'nome': "Init",
    'values': [
      "Esempio 0",
      "Esempio 1",
      "Esempio 2"
    ],
    'required': false,
  };

  @Input() valore: string;
  @Input() required: boolean;
  @Input() isFiltroTemplate: boolean = false;

  @Output() notifyChange: EventEmitter<string> = new EventEmitter<string>();
  @Output() aggiornaValore: EventEmitter<string> = new EventEmitter<string>();
  @Output() aggiornaGruppo: EventEmitter<string> = new EventEmitter<string>();

  aggiorna(val) {
    this.aggiornaValore.emit(val);
    this.isSelezione = false;    
  }
  
  typeaheadOnSelect($event) {
    // selected object is $event.item
    //console.log("ECCOCI");
    //console.log($event.item);
    
    this.selected = $event.item['valore'];    
    this.aggiornaGruppo.emit($event.item['gruppo']);
    
    this.isSelezione = true;


    //Reminder: this.config['campo']
    /*
    * Controllare che il valore corrisponda a uno degli elementi
    * della lista.
    *
    * P.S. Questo nel caso inserisca a mano invece di selezionare 
    * uno dei suggerimenti.    
    */

  }
}
