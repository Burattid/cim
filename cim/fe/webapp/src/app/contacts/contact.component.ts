import { AfterViewInit, Component, ViewEncapsulation } from '@angular/core';
import {CimApiService} from '../services/cim.service';
import {ActivatedRoute} from "@angular/router";
declare let jQuery: any;

@Component({
  selector: 'app-contact',
  templateUrl: './contact.template.html',
  encapsulation: ViewEncapsulation.None
})
export class ContactComponent implements AfterViewInit{

  public contact : Contact;
  public prova;
  public isLoading : boolean = false;
  private id : string;

  constructor(private _demoService: CimApiService, private route: ActivatedRoute) { 
    this.route.params.subscribe( params => {this.id = params['id']} );
  }

  ngAfterViewInit(): void {
    jQuery('.parsleyjs').parsley();
  }

  ngOnInit() {
    //page load
    this.getContact();

    this._demoService.getProva().subscribe(
      data => { this.prova = data, this.isLoading = false},
      err => console.error(err),
      () => console.log(this.prova)
    );
  }

  save(){
    console.log(this.contact);
  }

  getContact() {
    this.isLoading = true;
      this._demoService.getContact(this.id).subscribe(
        data => { this.contact = new Contact(2,"sss"), this.isLoading = false},
        err => console.error(err),
        () => console.log(this.contact)
      );
    }

}




export class Contact {

  constructor(
    public qta: number,
    public name: string
  ) {  }

}