import { Component } from '@angular/core';
import {CimApiService} from '../services/cim.service';


@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.template.html'
})
export class ContactsComponent {

  public contacts;
  public isLoading : boolean = false;
  public searchQuery : string;

  constructor(private _demoService: CimApiService) { }

  ngOnInit() {
    //page load
  }

  search(){
    this.getContacts();
  }

  getContacts() {
      this.isLoading = true;
       this._demoService.getContacts(this.searchQuery).subscribe(
          data => { this.contacts = data, this.isLoading = false},
          err => console.error(err),
          () => console.log('done loading contacts')
        );
      }

}
