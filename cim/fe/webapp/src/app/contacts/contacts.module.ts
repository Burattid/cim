import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { ContactsComponent } from './contacts.component';
import {CimApiService} from '../services/cim.service';

import { FormsModule } from '@angular/forms';


export const routes = [
  { path: '', component: ContactsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [ CommonModule, RouterModule.forChild(routes), FormsModule ],
  declarations: [ ContactsComponent ],
  providers: [CimApiService],
})
export class ContactsModule {
  static routes = routes;
}
