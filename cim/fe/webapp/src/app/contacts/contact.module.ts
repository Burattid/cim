import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { ContactComponent } from './contact.component';
import {CimApiService} from '../services/cim.service';

import { FormsModule } from '@angular/forms';


export const routes = [
  { path: '', component: ContactComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [ CommonModule, RouterModule.forChild(routes), FormsModule ],
  declarations: [ ContactComponent ],
  providers: [CimApiService],
})
export class ContactModule {
  static routes = routes;
}
