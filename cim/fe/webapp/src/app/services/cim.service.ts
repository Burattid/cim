import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CimApiService {

    constructor(private http: HttpClient) { }

    private ENV: string = 'stub';
    private CONFIG_API = {
        'stub': {
            "login": "assets/demo/login.json",
            "contacts": "assets/demo/contacts.json",
            "contact": "assets/demo/contatto.json",
            "save-contact": "assets/demo/",
            "prova": "api/prova/",
            "imputazioni": "assets/demo/imputazioni.json"
        },
        'remote': {
            "login": "api/?action=login",
            "logout": "api/?action=logout",
            "contacts": "api/contacts",
            "contact": "api/contact",
            "save-contact": "api/=save-contact",
        }
    };

    private getApi(action: string): string {
        return window.location.protocol + "//" + window.location.host + "/" + this.CONFIG_API[this.ENV][action];
    }

    /**
     * 
     * @param query 
     */
    getContacts(query: string) {
        return this.http.get(this.getApi('contacts') + '?s=' + query);
    }

    getContact(id: string) {
        return this.http.get('/assets/demo/contact.json?id=' + id);
    }
    saveContact(contact) {
        let body = JSON.stringify(contact);
        return this.http.post('/api/food/', body, httpOptions);
    }

    getProva() {
        return this.http.get(this.getApi('prova'));
    }

    getImputazioni() {
                
        return this.http.get(this.getApi('imputazioni'));
    }
}

