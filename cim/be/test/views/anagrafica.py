import logging.config

from django.views import View

from be.test.services.anagrafica import AnagraficaService
from be.core import utils
from be.core.decorator import response_handler

log = logging.getLogger('cim')

class AnagraficaView(View):

    anagraficaService = AnagraficaService()

    @response_handler
    def index(self, request):

        anagrafica = self.anagraficaService.getAnagraficaByApplicaSuColori("quals")

        return utils.get_model_JSON(anagrafica)
