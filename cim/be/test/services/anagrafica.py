from be.test.models.anagrafica import Anagrafica

class AnagraficaService():

    def getAnagraficaById(self, id):

        return Anagrafica.objects.get(id_anagrafica_prova=id)

    def getAnagraficaByApplicaSuColori(self, applica_su_colori):

        return Anagrafica.objects.getAnagraficaByApplicaSuColori(applica_su_colori)
