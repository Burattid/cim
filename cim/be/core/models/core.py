import logging.config
from django.db import models, connection
from django.db.models.options import Options

log = logging.getLogger('cim')

class Core(models.Model):

    class Meta:
        app_label = 'core_cim'

    @classmethod
    def _create_custom_model(cls, name, pk, fields=None, app_label='CustomModel', module='', options=None, admin_opts=None):
        return cls.__create_custom_model(name, pk, fields, app_label, module, options, admin_opts)

    @classmethod
    def __create_custom_model(cls, name, pk, fields, app_label, module, options, admin_opts):

        """
        Create custom model
        """

        class Meta:
            # Using type('Meta', ...) gives a dictproxy error during model creation
            pass

        if app_label:
            # app_label must be set using the Meta inner class
            setattr(Meta, 'app_label', app_label)

        # Update Meta with any options that were provided
        if options is not None:
            for key, value in options.iteritems():
                setattr(Meta, key, value)

        # Set up a dictionary to simulate declarations within a class
        attrs = {'__module__': module, 'Meta': Meta}

        # Add in any fields that were provided
        if fields:
            attrs.update(fields)

        # Create the class, which automatically triggers ModelBase processing
        #model = CustomModel(attrs)
        model = type(name, (CustomModel,), attrs)()
        model.opts = attrs
        model.pk = pk

        return model

class CustomModel(models.Model):

    class Meta:
        app_label = 'custom_model_cim'