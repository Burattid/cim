from django.conf.urls import url
from django.urls import path, re_path
from django.contrib import admin
from be import views
from be.test.views.anagrafica import AnagraficaView
from be.test.views.test_response import ResponseTestCase


anagraficaView = AnagraficaView()

urlpatterns = [
	url(r'^admin/', admin.site.urls),

	#test error
	url('prova/', anagraficaView.index),
	url('prova2/', ResponseTestCase.test_200_response),
	url('test200/', ResponseTestCase.test_200_response),
	url(r'^test200parameter/(?P<name>[-\w]+)/$', ResponseTestCase.test_200_parameter_response),
	url('test200argument/', ResponseTestCase.test_200_parameter_argument),
	url('test404/', ResponseTestCase.test_404),
	url('test500/', ResponseTestCase.test_500_response),
	url('test403/', ResponseTestCase.test_403_response),
	url('test302/', ResponseTestCase.test_302_response),

]
